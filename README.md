# vim-lsp for Pearl

async language server protocol plugin for vim and neovim

## Details

- Plugin: https://github.com/prabirshrestha/vim-lsp
- Pearl: https://github.com/pearl-core/pearl
